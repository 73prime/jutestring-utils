# Jutestring Utils
- [x] Uppercase (/upper)
- [x] Lowercase (/lower)
- [x] Altcase (/altcase)
- [x] Leet (/leet)
- [x] URL Shortener (/seturl & /geturl)

## Things
* Building: `make build`
* Running: `make run`
