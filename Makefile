.PHONY: default
default: displayhelp ;

displayhelp:
	@echo Use "clean, showcoverage, tests, build, buildlinux or run" with make, por favor.

showcoverage: tests
	@echo Running Coverage output
	go tool cover -html=coverage.out

tests: clean
	@echo Running Tests
	go test --coverprofile=coverage.out ./...

docker:
	docker build -t jutestring:latest . -f Dockerfile
	docker run -it --env PORT=3333 -p 80:3333 jutestring:latest

run: build
	@echo Running program
	./bin/jutestring

build:
	@echo Running build command
	go build -o bin/jutestring src/main.go src/website.go src/leet.go src/urlhandler.go

clean:
	@echo Removing binary TODO
	rm -rf ./bin ./vendor Gopkg.lock
