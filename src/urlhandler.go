package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"html/template"
	"io/ioutil"
	"log"
	"math/rand"
	"time"
)

type UrlStruct struct {
	Shortcode string `json:"shortcode"`
	Url       string `json:"url"`
}

type UrlList []UrlStruct
type UrlHandler struct {
}

func GetURLHTML() string {
	html := `
<html>
<head>
<meta http-equiv="Refresh" content="0; URL={{.LongUrl}}" />
</head>
</html>
`
	return html
}
func RandomString(n int) string {
	rand.Seed(time.Now().UnixNano())
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}
func RetrieveUrlList() UrlList {
	var payload UrlList
	content, err := ioutil.ReadFile(fmt.Sprintf("%s/urls.json", viper.GetString("datafolder")))
	if err != nil {
		log.Printf("Error when opening file for retrieving list of URLs: %s", err)
	} else {
		// Now let's unmarshall the data into `payload`
		err = json.Unmarshal(content, &payload)
		if err != nil {
			log.Printf("Error during Unmarshal(): %s", err)
		}
	}
	return payload
}

func RetrieveURL(list UrlList, shortcode string) string {
	for _, val := range list {
		if val.Shortcode == shortcode {
			return val.Url
		}
	}
	return ""
}
func (ls *UrlHandler) setUrl(origURL string) string {
	shortcode := RandomString(8)
	list := RetrieveUrlList()
	if RetrieveURL(list, shortcode) != "" {
		log.Println("Shortcode already used; try again")
		return ""
	}

	log.Printf("No Shortcode found for %s, attempting to store", shortcode)
	uStruct := UrlStruct{
		Shortcode: shortcode,
		Url:       origURL,
	}
	list = append(list, uStruct)
	file, _ := json.MarshalIndent(list, "", " ")

	err := ioutil.WriteFile(fmt.Sprintf("%s/urls.json", viper.GetString("datafolder")), file, 0644)
	if err != nil {
		log.Printf("Error trying to store shortcode %s: %s", shortcode, err.Error())
		return ""
	}
	log.Printf("Stored %s", shortcode)
	return shortcode
}

type UIURLData struct {
	LongUrl string
}

func (ls *UrlHandler) getUrlError() string {
	html := "<html><p>error</p></html>"
	tmpl := template.Must(template.New("myname").Parse(html))
	var tpl bytes.Buffer
	tmpl.Execute(&tpl, nil)
	return tpl.String()
}
func (ls *UrlHandler) getUrl(code string) string {
	log.Printf("Shortcode: %s\n", code)
	list := RetrieveUrlList()
	url := RetrieveURL(list, code)

	if url == "" {
		return ls.getUrlError()
	}
	//good url
	log.Printf("Found url for shortcode %s: %s", code, url)
	tmpl := template.Must(template.New("myname").Parse(GetURLHTML()))
	data := UIURLData{
		LongUrl: url,
	}
	var tpl bytes.Buffer
	tmpl.Execute(&tpl, data)
	return tpl.String()
}
