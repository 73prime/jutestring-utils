package main

import (
	"strings"
	"testing"
)

func checkError(err error, t *testing.T) {
	if err != nil {
		t.Errorf("An error occurred: #{err}")
	}
}
func TestGetUIDisplay(t *testing.T) {
	actual := UIDisplay()
	expected := `
<html>
<head>
<style>
html {
	background-color: #39424e;
}
h1, h2, h3 {
	margin-top: 2em;
	margin-bottom: .5em;
	font-family: didact gothic,sans-serif;
	opacity: .6;
}
body {
  	font-size: 16px;
	font-family: didact gothic,sans-serif;
	color: #fff;
	line-height: 2rem;
	letter-spacing: 1.5px;
	text-shadow: none;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: left;
	margin: 40px;
	opacity: 1;
}
* {
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}
</style>
</head>
<body>
<h1>jutestring - your portal to string manipulations</h1>
<ul>
<li>leetspeak: hit base URL with /leet.</li>
<li>aLtCaSe: hit base URL with /altcase.</li>
<li>UPPERCASE: hit base URL with /upper.</li>
<li>lower: hit base URL with /lower.</li>
<li>reverse: hit base URL with /reverse.</li>
<li>seturl: gets short url for longer one. Use /seturl</li>
<li>geturl: gets long url for shorter one. Use /geturl</li>
</ul>
</body>
</html>
`
	actual = strings.Replace(actual, "\n", "", -1)
	actual = strings.Replace(actual, "\t", "", -1)
	actual = strings.Replace(actual, " ", "", -1)
	expected = strings.Replace(expected, "\n", "", -1)
	expected = strings.Replace(expected, "\t", "", -1)
	expected = strings.Replace(expected, " ", "", -1)
	if actual != expected {
		t.Errorf("Actual: \n%s\n=====\nExpected:\n%s", actual, expected)
	}
}
