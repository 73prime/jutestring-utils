package main

import (
	"encoding/json"
	"errors"
	"fmt"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"io"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"
	"unicode"
)

const formattedString = "%s"

func initConfig() (err error) {
	log.Printf("Starting jutestring, getting config")
	// Find home directory.
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		return err
	}
	viper.AddConfigPath(home)
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("~")
	viper.SetConfigName("jutestring.toml")
	pflag.Int("PORT", 8888, "This is the port to bind to. 8888 is default.")
	pflag.String("DATAFOLDER", ".", "This is the folder for data to be stored.")
	pflag.String("BASEURL", "http://jutestring.com", "This is the base URL returned with shortcode.")
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	if err != nil {
		fmt.Println("Uh oh. no config file")
		err = nil
	}
	fmt.Printf("Will use %d as port number.\n", viper.GetInt("port"))
	if viper.GetInt("port") == 0 {
		err = errors.New("No valid port number")
	}
	fmt.Printf("Will use %s as data folder.\n", viper.GetString("datafolder"))
	if viper.GetString("datafolder") == "" {
		err = errors.New("No valid data folder")
	}
	return err
}

func RootHandler(w http.ResponseWriter, r *http.Request) {
	html := UIDisplay()
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, html)
}
func CommonHandler(w http.ResponseWriter, text string, response_type string) {
	data, _ := json.Marshal(map[string]string{
		"response_type": response_type,
		"text":          fmt.Sprintf(formattedString, text),
	})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(data)
}
func UpperHandler(w http.ResponseWriter, r *http.Request) {
	text := r.FormValue("text")
	text = strings.ToUpper(text)
	CommonHandler(w, text, "ephemeral")
}
func LowerHandler(w http.ResponseWriter, r *http.Request) {
	text := r.FormValue("text")
	text = strings.ToLower(text)
	CommonHandler(w, text, "ephemeral")
}
func AltHandler(w http.ResponseWriter, r *http.Request) {
	text := r.FormValue("text")
	newtext := ""
	for index, character := range text {
		if index%2 == 0 {
			character = unicode.ToUpper(character)
		} else {
			character = unicode.ToLower(character)
		}
		newtext = newtext + string(character)
	}
	CommonHandler(w, newtext, "ephemeral")
}
func LeetHandler(w http.ResponseWriter, r *http.Request) {
	leetSpeaker := new(LeetSpeaker)
	leetSpeaker.Init()
	results := leetSpeaker.translateToLeet(r.FormValue("text"))
	CommonHandler(w, results, "in_channel")
}
func ReverseHandler(w http.ResponseWriter, r *http.Request) {
	text := r.FormValue("text")
	runes := []rune(text)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	CommonHandler(w, string(runes), "in_channel")
}
func GetUrlHandler(w http.ResponseWriter, r *http.Request) {
	handler := new(UrlHandler)
	//v := url.Values{}
	code := r.URL.Query().Get("id")
	results := ""
	//w.Header().Set("Content-Type", "application/text")
	if code == "" {
		w.WriteHeader(500)
		results = handler.getUrlError()
	} else {
		results = handler.getUrl(code)
		w.WriteHeader(200)
	}
	w.Write([]byte(results))
}
func SetUrlHandler(w http.ResponseWriter, r *http.Request) {
	handler := new(UrlHandler)
	results := handler.setUrl(r.FormValue("text"))
	if results != "" {
		results = fmt.Sprintf("%s/geturl?id=%s", viper.GetString("BASEURL"), results)
	} else {
		results = "There was an issue storing this URL."
	}
	CommonHandler(w, results, "ephemeral")
}
func handleLocalRequests() {
	http.HandleFunc("/", RootHandler)
	http.HandleFunc("/upper", UpperHandler)
	http.HandleFunc("/lower", LowerHandler)
	http.HandleFunc("/altcase", AltHandler)
	http.HandleFunc("/leet", LeetHandler)
	http.HandleFunc("/seturl", SetUrlHandler)
	http.HandleFunc("/geturl", GetUrlHandler)
	http.HandleFunc("/reverse", ReverseHandler)
	log.Printf("Started Webserver on port %d.\n", viper.GetInt("port"))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", viper.GetInt("port")), nil))
}
func main() {
	rand.Seed(time.Now().Unix())
	err := initConfig()
	if err == nil {
		log.Printf("Initialized...")
		handleLocalRequests()
	} else {
		log.Fatalln(err.Error())
	}
}
