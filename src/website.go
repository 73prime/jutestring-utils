package main

import (
	"bytes"
	"html/template"
)

func GetUIHTML() string {
	html := `
<html>
<head>
<style>
html {
	background-color: #39424e;
}
h1, h2, h3 {
	margin-top: 2em;
	margin-bottom: .5em;
	font-family: didact gothic,sans-serif;
	opacity: .6;
}
body {
  	font-size: 16px;
	font-family: didact gothic,sans-serif;
	color: #fff;
	line-height: 2rem;
	letter-spacing: 1.5px;
	text-shadow: none;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: left;
	margin: 40px;
	opacity: 1;
}
* {
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}

</style>
</head>
<body>
<h1>{{.PageTitle}}</h1>
<ul>
{{range .ListOfGoods}}
	<li>
    {{.}}
	</li>
{{end}}
</ul>
</body>
</html>
`
	return html
}

type UIData struct {
	PageTitle   string
	ListOfGoods []string
}

func UIDisplay() string {

	tmpl := template.Must(template.New("myname").Parse(GetUIHTML()))

	data := UIData{
		PageTitle: "jutestring - your portal to string manipulations",
	}

	data.ListOfGoods = append(data.ListOfGoods, "leetspeak: hit base URL with /leet.")
	data.ListOfGoods = append(data.ListOfGoods, "aLtCaSe: hit base URL with /altcase.")
	data.ListOfGoods = append(data.ListOfGoods, "UPPERCASE: hit base URL with /upper.")
	data.ListOfGoods = append(data.ListOfGoods, "lower: hit base URL with /lower.")
	data.ListOfGoods = append(data.ListOfGoods, "reverse: hit base URL with /reverse.")
	data.ListOfGoods = append(data.ListOfGoods, "seturl: gets short url for longer one. Use /seturl")
	data.ListOfGoods = append(data.ListOfGoods, "geturl: gets long url for shorter one. Use /geturl")
	var tpl bytes.Buffer
	tmpl.Execute(&tpl, data)
	return tpl.String()
}
